---
title: Quién soy
subtitle: Conoce más acerca de mí
comments: false
---

Mi nombre es Juan Febles y me encanta enseñar aprendiendo y aprender enseñando.  

- Soy maestro de Educación Especial (Profesor de Apoyo) en un colegio de Tenerife
- Me encanta el [Software Libre](https://www.gnu.org/philosophy/free-sw.es.html)
- Realizo un Podcast sobre GNU/Linux: [Podcast Linux](https://podcastlinux.com/)

Cacharreo y pruebo todo lo que puedo y me gusta conocer y compartir lo que aprendo con los demás.
