---
title: Mis proyectos
subtitle: A lo que le doy vueltas y me apasiona
comments: true
---

Me gusta mucho probar y tratear con nuevas ideas y cacharros y compartir todo lo aprendido.  

En referencia al podcasting:  

+ Podcast [Onda Salle](https://www.ivoox.com/podcast-onda-salle_sq_f1151497_1.html) (actualmente parado)
+ Podcast [Tagoror Educativo](https://www.ivoox.com/podcast-tagoror-educativo-podcast_sq_f1251790_1.html) (finalizado) 
+ [Podcast Linux](https://podcastlinux.com/)
+ [Podcast Linux Express](https://podcastlinux.com/)
+ [Podcast Maratón Linuxero](https://maratonlinuxero.org/)
+ [Charla podcasting](https://twitter.com/podcastlinux/status/887995809534545921?s=20) y [Educación y Software Libre](https://osl.ull.es/eventos/jornadas-ensenanza-y-software-libre/)   (2017)
+ [Curso online de Audacity](https://www.youtube.com/watch?v=GlvLiol6iN8&list=PLdt4gHTaH61FH8It6H99ZuqCPIPff-zmF)
+ [Curso de podcasting](https://podcastlinux.com/cursopodcasting/)
+ [Taller de podcasting](https://podcastlinux.com/tallerpodcasting18/) en [Linux Center](https://linuxcenter.es/component/k2/item/149-taller-de-podcasting)
+ [Taller de podcasting](https://podcastlinux.com/tallerpodcasting/) en [TLP2019](https://twitter.com/ULLenTLP/status/1151811487683555328)
+ Curso [Audacity](https://www.audacityteam.org/) para la [Asociación Podcast](http://www.asociacionpodcast.es/curso-audacity-por-juan-febles/)


Proyectos colaborativos:  

+ [Maratón Linuxero](https://maratonlinuxero.org/)
+ [Flisol Tenerife](https://flisol.info/FLISOL2019/Espana/Tenerife)
+ [Wifiteca](https://wifiteca.gitlab.io)
+ [Raspberry Weather Station](http://lasallelalaguna.es/meteosalle19)

Cursos vídeotutoriales online abiertos:  

+ [Podcasting libre](https://www.youtube.com/watch?v=a6dCJpjLH3Q&list=PLdt4gHTaH61HOOLsyAc2xYzdbinem9ooZ)
+ [Vídeo libre](https://www.youtube.com/watch?v=ujdeOkz4TFY&list=PLdt4gHTaH61EEvqTiD50zZDHgwQsKhjq2)

Web:  

+ Creación de webs estáticas a través de [Jekyll y Gitlab](https://gitlab.com/frentealmicrofono/frentealmicrofono.gitlab.io)
+ Creación de webs estáticas a través de [Hugo y Gitlab](https://gitlab.com/juanfebles/juanfebles.gitlab.io)

Otros:  

+ [Acuario colegial](http://lasallelalaguna.es/acuario)
